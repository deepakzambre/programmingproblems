﻿namespace CrackingProgrammingInterviews.Fundamentals
{
    using System;

    /*
     * Given n, find x st x^2 <= n^2 < (x + 1)^2
     */
    public class ApproximateSquareRoot
    {
        private static int binSearch(int lower, int upper, int num)
        {
            int ret = 0;
            int l = lower;
            int u = upper;

            while (ret != l && ret != u)
            {
                int m = (l + u)/2;
                if (m*m < num)
                {
                    ret = m;
                    l = m + 1;
                }
                else if (m*m == num)
                {
                    ret = m;
                    break;
                }
                else
                {
                    u = m - 1;
                }
            }

            return ret;
        }

        public static int GetApproximateSquareRoot(int num)
        {
            if (num <= 0)
            {
                return 0;
            }

            int upper = 1;
            for (; upper * upper < num; upper = 2 * upper);
            return binSearch(upper / 2, upper, num);
        }

        public static void Driver(string[] args)
        {
            Console.WriteLine(GetApproximateSquareRoot(int.Parse(args[0])));
        }
    }
}

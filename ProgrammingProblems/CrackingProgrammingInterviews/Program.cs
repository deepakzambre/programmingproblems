﻿namespace CrackingProgrammingInterviews
{
    class Program
    {
        static void Main(string[] args)
        {
            Fundamentals.ApproximateSquareRoot.Driver(args);
        }
    }
}
